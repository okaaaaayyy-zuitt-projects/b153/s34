// import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap'

export default function CourseCard({courseProp}){

	const {_id, name, description, price} = courseProp

	// const [stateName, setStateName] = useState(default value)

	// const [count, setCount] = useState(0)
	// const [seats, setSeats] = useState(30)
	// const [isOpen, setIsOpen] = useState(true)

// ACTIVITY

	// function enroll(){
	// 	if(seats !== 0){
	// 		setCount(count + 1)
	// 		setSeats(seats - 1)
	// 		// console.log(`Enrollees:` + count)
	// 		// console.log(seats)			
	// 	}else{
	// 		alert("No more seats")
	// 	}
	// }

	// useEffect allows us to execute code when a given state(s) (inside the array) changes

	// when a component mounts (loads for the user for the first time), all of its associated states are initialized/created.
	// Initialization counts as a state change, so useEffect fires/activates

	// The array inside of a useEffect hook can contain 0 states, 1 state, or many states (but the array must always be present)

	// If the array is empty, useEffect will ONLY  happen when component mount (when it first loads) 

	// If the array has one state named, useEffect will only fire when that state changes AND when the state changes

	// If there are two or more states in the array, useEffect will fire when the component mounts and when ANY of those state change

	// useEffect(() => {
	// 	if(seats === 0){
	// 		setIsOpen(false)
	// 	}
	// }, [seats])


	// const enroll = () => {
	// 		setCount(count + 1)
	// 		setSeats(seats - 1)	
	// 	}

	return(
		<Card className="m-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
{/*				<Card.Text>Enrollees: {count}</Card.Text>
				<Card.Text>Seats: {seats}</Card.Text>
				{isOpen
					?
				<Button variant="primary" onClick={enroll}>Enroll</Button>						
				:
				<Button variant="primary" disabled>Enroll</Button>				
				}*/}
			</Card.Body>
		</Card>
	)
}